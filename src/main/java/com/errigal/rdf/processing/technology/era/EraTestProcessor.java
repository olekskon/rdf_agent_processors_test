package com.errigal.rdf.processing.technology.era; 
/**
 * Copyright (c) 2020 Errigal Inc.
 *
 * This software is the confidential and proprietary information of Errigal, Inc.  You shall not disclose such confidential information and shall use it only in accordance with the license agreement
 * you entered into with Errigal.
 *
 *
 */
/**
 * User: Andrew Kavanagh <andrew.kavanagh@errigal.com> Date: 2020-10-29
 */

import com.errigal.rdf.dto.IncomingMessage;
import com.errigal.rdf.processing.base.config.ConfigProcessor;
import com.errigal.rdf.processing.base.performance.PerformanceProcessor;
import com.errigal.rdf.processing.request.Request;
import com.errigal.rdf.processing.request.Task;
import com.errigal.rdf.processing.response.CommonDeviceTopology;
import com.errigal.rdf.processing.response.error.Errors;
import com.errigal.rdf.processing.response.generic.ConfigNode;
import com.errigal.rdf.processing.response.generic.ConfigNodeType;
import com.errigal.rdf.processing.response.generic.Parameter;
import com.errigal.rdf.processing.technology.era.parameter.EraParameter;
import com.errigal.rdf.processing.technology.era.parameter.EraParameter.EraParameterDiscoveryType;
import com.errigal.rdf.processing.technology.era.parameter.EraParameterMapping;
import com.errigal.rdf.processing.technology.era.parameter.EraParameterMapping.EraDiscoveredNameParseType;
import com.errigal.rdf.processing.technology.era.task.EraSnmpTaskCreator;
import com.errigal.rdf.processing.technology.era.task.EraSnmpTaskProcessor;
import com.errigal.rdf.protocols.snmp.SNMPTable;
import com.errigal.rdf.protocols.snmp.SNMPVarbind;
import com.errigal.rdf.util.SupportedVersion;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EraTestProcessor implements ConfigProcessor, PerformanceProcessor {

  private CommonDeviceTopology cdt = new CommonDeviceTopology();
  private final String TECHNOLOGY_NAME = "ANDREWS_IONE";
  private final String FROM_VERSION = "2.7.0.1";
  private final String TO_VERSION = "2.8.0.134";
  private Errors errors = new Errors();
  private boolean stopped = false;
  private SupportedVersion supportedVersion = new SupportedVersion(FROM_VERSION, TO_VERSION);
  private EraSnmpTaskProcessor eraSnmpTaskProcessor = new EraSnmpTaskProcessor();

  @Override
  public CommonDeviceTopology getConfigs() {
    return cdt;
  }

  @Override
  public CommonDeviceTopology getPerformance() {
    return cdt;
  }


  public Request buildRequest(IncomingMessage incomingMessage) {
    log.info("Building request......");
    if (incomingMessage.getSnmpCredentials() != null && incomingMessage.getSnmpCredentials().getSnmpVersion() != null) {
      incomingMessage.setSnmpVersion(incomingMessage.getSnmpCredentials().getSnmpVersion());
      incomingMessage.setSnmpCommunity(incomingMessage.getSnmpCredentials().getReadCommunity() != null ? incomingMessage.getSnmpCredentials().getReadCommunity() : "public");
      incomingMessage.setSnmpWriteCommunity(incomingMessage.getSnmpCredentials().getWriteCommunity() != null ? incomingMessage.getSnmpCredentials().getWriteCommunity() : "private");
      incomingMessage.setSnmpPort(incomingMessage.getSnmpCredentials().getInternalPort() != null ? incomingMessage.getSnmpCredentials().getInternalPort() : 162);
    }
    return getRequest(incomingMessage);
  }

  public Request getRequest(IncomingMessage incomingMessage) {
    EraSnmpTaskCreator snmpTaskCreator = new EraSnmpTaskCreator(incomingMessage);
    log.info("Creating tasks for ERA discovery request......");
    String discoveredName = incomingMessage.getQueryParams().get("discoveredName") == null || incomingMessage.getQueryParams().get("discoveredName").equals("") ? "ERA_Controller"
        : incomingMessage.getQueryParams().get("discoveredName");
    cdt.setDiscoveredName(discoveredName);
    return Request.builder()
        .poll(Collections.emptyList())
        .discoveryTasks(snmpTaskCreator.getTasks()).build();
  }

  @Override
  public Stack<Task> processResponse(Task task, Task nextTask) {
    Stack<Task> followUpTasks = new Stack<>();
    processTask(task);
    return followUpTasks;
  }

  public void processTask(Task task) {
    if (task.hasErrors()) {
      errors.addErrors(task.getErrors());
    } else if (task.getType().isSNMP()) {
      List<EraParameter> eraParams = eraSnmpTaskProcessor.processSnmpParameterTask(task);
      eraParams.forEach(this::addEraParameterToCdt);
    }
  }

  private void addEraParameterToCdt(EraParameter eraParamSpec) {
    if (eraParamSpec != null && eraParamSpec.getType() == EraParameterDiscoveryType.PERFORMANCE) {
      Parameter param = Parameter.builder().key(eraParamSpec.getKey())
          .prettyName(eraParamSpec.getPrettyName())
          .currentValue(eraParamSpec.getParamValue())
          .newValue(eraParamSpec.getParamValue())
          .tags(eraParamSpec.getParameterTag())
          .unit(eraParamSpec.getUnitType()).build();
      CommonDeviceTopology element = getCommonDeviceTopologyByDiscoveredName(eraParamSpec.getDiscoveredName());
      element.addParameter(param);
    } else if (eraParamSpec != null && eraParamSpec.getType() == EraParameterDiscoveryType.CONFIG) {
      ConfigNode config = new ConfigNode();
      config.setKey(eraParamSpec.getKey());
      config.setLabel(eraParamSpec.getPrettyName());
      config.setValue(eraParamSpec.getParamValue());
      config.setTags(eraParamSpec.getParameterTag());
      config.setUnit(eraParamSpec.getUnitType());
      config.setWritable(eraParamSpec.isEditable());
      config.setType(ConfigNodeType.RAW_VALUE);
      CommonDeviceTopology element = getCommonDeviceTopologyByDiscoveredName(eraParamSpec.getDiscoveredName());
      element.addConfig(config);
    }
  }

  private CommonDeviceTopology getCommonDeviceTopologyByDiscoveredName(String discoveredName) {
    CommonDeviceTopology result;
    if (discoveredName.equals("1.0")) {
      result = cdt;
    } else {
      CommonDeviceTopology child = cdt.findByDiscoveredName(discoveredName);
      if (child == null) {
        child = new CommonDeviceTopology();
        child.setDiscoveredName(discoveredName);
        cdt.addChild(child);
      }
      result = child;
    }
    return result;
  }

  @Override
  public boolean isCompatible(String name, String version) {
    boolean supported = false;
    if (name.equals(TECHNOLOGY_NAME) && supportedVersion.isCompatible(version)) {
      supported = true;
    }
    return supported;
  }

  @Override
  public Errors getErrors() {
    return errors;
  }

  @Override
  public boolean stopped() {
    return stopped;
  }
}
